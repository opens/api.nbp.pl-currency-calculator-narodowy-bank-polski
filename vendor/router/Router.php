<?php

namespace Vendor\router;

class Router
{
    private static $routes = [];

    public static function get($url, $controllerClass, $methodName)
    {
        self::$routes[] = [
            'method' => 'GET',
            'url' => $url,
            'controller' => $controllerClass,
            'method_name' => $methodName
        ];
    }

    public static function post($url, $controllerClass, $methodName)
    {
        self::$routes[] = [
            'method' => 'POST',
            'url' => $url,
            'controller' => $controllerClass,
            'method_name' => $methodName
        ];
    }

    public static function dispatch($url='', $method)
    {
        foreach (self::$routes as $route) {
            if ($route['url'] === $url && $route['method'] === $method) {
                $controller = new $route['controller']();
                $method = $route['method_name'];
                return $controller->$method();
            }
        }
        header('HTTP/1.0 404 Not Found');
        die('404 Not Found');
    }
}