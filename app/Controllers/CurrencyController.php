<?php

namespace App\Controllers;

use App\Models\Currency;
use Vendor\config\Config;
use Vendor\view\View;

class CurrencyController
{
    public function fetchAndSyncAll()
    {
        $urls = Config::get('urls');
        foreach ($urls as $url) {
            $this->fetchAndSync($url);
        }
    }

    public function fetchAndSync($url)
    {
        $data = file_get_contents($url);
        if (empty($data)) {
            return null;
        }
        $data = json_decode($data, true);

        if (!empty($data) && !empty($data[0]['rates'])) {
            foreach ($data[0]['rates'] as $currencyData) {
                Currency::insertOrIgnore($currencyData);
            }
        }
    }

    public static function index()
    {
        $data = [
            'heading' => 'headingdfg',
            'content' => 'headingdfg',
            'currencies' => Currency::get(),
        ];

        return View::render('index', $data);
    }

    public static function сalculator()
    {
        return View::render('сalculator');
    }
}
