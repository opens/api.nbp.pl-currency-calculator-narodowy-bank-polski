<?php
header('charset=utf-8');
mb_internal_encoding('UTF-8');
$envFile = __DIR__ . '/../.env';
if (file_exists($envFile)) {
    $envData = parse_ini_file($envFile, false, INI_SCANNER_RAW);
    if (is_array($envData))
        foreach ($envData as $key => $value) {
            putenv("$key=$value");
        }
}

spl_autoload_register(function ($className) {
    $baseDir = __DIR__ . '/../';
    $file = $baseDir . $className . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
});