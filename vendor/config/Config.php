<?php

namespace Vendor\config;
class Config
{
    private static $config = null;

    public static function get($key, $default = null)
    {
        if (self::$config === null) {
            self::$config = require_once __DIR__ . '/../../config/app.php';
        }

        return isset(self::$config[$key]) ? self::$config[$key] : $default;
    }
}