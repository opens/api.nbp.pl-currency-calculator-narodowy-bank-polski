<?php

namespace Vendor\View;

class View
{
    static $views = [];
    static $sections = [];
    static $extendField = false;
    static $view = null;
    static $yieldFields = [
//        'content','title'
    ];
    static $data = [];

    public static function render($view, $data = [])
    {
        static::$data = $data;
        static::$view = $view;
        static::$views[$view] = static::getView($view);
        static::$extendField = static::getExtendNameFromViews();
        if (!empty(static::$extendField))
            static::$views[static::$extendField] = static::getView("layout/" . static::$extendField);

        static::$sections[] = static::parseAllSectionsInViews();
        static::$yieldFields = static::parseAllYieldFieldsInViews();

        static::replaceYieldFieldsValuesFromSectionsInViews();
        extract(static::$data);
        $code = static::getCodeToExecute();
        ob_start();
        eval(' ?>' . $code . '<?php ');
        $result = ob_get_clean();

        return $result;
    }

    public static function getCodeToExecute()
    {
        return static::$views[!empty(static::$extendField) ? static::$extendField : static::$view];
    }

    private static function replaceYieldFieldsValuesFromSectionsInViews()
    {
        foreach (static::$yieldFields as $yieldField) {
            if (isset(static::$sections[0][$yieldField])) {
                $value = static::$sections[0][$yieldField];
                $pattern = '/@yield\(\'' . preg_quote($yieldField) . '\'\)/';
                static::$views[static::$extendField] = preg_replace($pattern, $value, static::$views[static::$extendField]);
            }
        }
    }


    private static function getView($name)
    {
        $filePath = __DIR__ . "/../../Resources/views/" . $name . ".blade.php";
        $contents = '';
        $handle = fopen($filePath, 'r');
        if ($handle) {
            while (!feof($handle)) {
                $contents .= fread($handle, 8192);
            }
            fclose($handle);
        }
        return $contents;
    }

    private static function getExtendNameFromViews()
    {
        $extendField = false;
        foreach (static::$views as $view) {
            preg_match('/@extends\(\'([^\'\s]*)\'\)/', $view, $matches);
            if (isset($matches[1])) {
                $extendField = $matches[1];
                break;
            }
        }
        return $extendField;
    }

    private static function parseAllSectionsInViews()
    {
        $sections = [];
        foreach (static::$views as $view) {
            preg_match_all('/@section\(\'(\w+)\'\)(.*?)@endsection/s', $view, $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $sectionName = $match[1];
                $sectionContent = $match[2];
                $sections[$sectionName] = $sectionContent;
            }
        }
        return $sections;
    }

    private static function parseAllYieldFieldsInViews()
    {
        $yieldFields = [];
        foreach (static::$views as $view) {
            preg_match_all('/@yield\(\'(.*)\'\)/', $view, $matches);
            foreach ($matches[1] as $match) {
                $yieldFields[] = $match;
            }
        }
        return $yieldFields;
    }
}
