@extends('app')
@section('content')
    <div style="overflow-x: auto; max-width: 100vw;">
        <table class="table table-hover table-striped ">
            <tr>
                <th>Waluta</th>
                <th>Kod waluty</th>
                <th>Kurs średni</th>
                <th>Kurs kupna</th>
                <th>Kurs sprzedaży</th>
            </tr>
            <?php foreach ($currencies as $currency): ?>
            <tr>
                <td><?= $currency['currency'] ?></td>
                <td><?= $currency['code'] ?></td>
                <td><?= $currency['mid'] ?></td>
                <td><?= $currency['bid'] ?></td>
                <td><?= $currency['ask'] ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
@endsection