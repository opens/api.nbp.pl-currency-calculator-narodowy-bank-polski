<?php

use App\Controllers\CurrencyController;

require_once __DIR__ . '/../../vendor/autoload.php';

(new CurrencyController())->fetchAndSyncAll();
