<?php
return [
    'urls' => [
        'https://api.nbp.pl/api/exchangerates/tables/A?format=json',
        'https://api.nbp.pl/api/exchangerates/tables/B?format=json',
        'https://api.nbp.pl/api/exchangerates/tables/C?format=json'
    ]
];