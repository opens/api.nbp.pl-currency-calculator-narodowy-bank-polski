<?php

namespace App\Models;

use mysqli;

class Model
{
    private static $instance = null;
    private $connection = null;
    private $conditions = '';
    public static $table = 'default_table';

    public static function test()
    {
        $t = self::getInstance();
        return $t;
    }

    private function __construct()
    {
        $this->connection = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));

        if ($this->connection->connect_error) {
            $this->dieWithError("Database connection error: " . $this->connection->connect_error);
        }
    }

//    private function __destruct()
//    {
//        $this->connection->close();
//    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public static function where($column, $operator, $value)
    {
        $instance = self::getInstance();
        $operator = $instance->sanitizeOperator($operator);
        $value = $instance->sanitizeValue($value);

        $condition = "{$column} {$operator} {$value}";

        return $instance->addCondition($condition);
    }

    public static function getTable()
    {
        return static::$table;
    }

    public static function get()
    {
        $instance = self::getInstance();
        $query = self::getQuery();

        $result = $instance->executeQuery($query);

        $rows = [];

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    public static function all()
    {
        return self::get();
    }

    public static function insert($data, $ignoreDuplicates = false)
    {
        $instance = self::getInstance();
        $data = $instance->normalizeData($data);

        foreach ($data as $row) {
            $columns = implode(', ', array_map(function ($column) {
                return "`$column`";
            }, array_keys($row)));

            $values = implode(', ', array_map([$instance, 'sanitizeValueAndQuote'], $row));

            $ignore = $ignoreDuplicates ? 'IGNORE' : '';

            $query = "INSERT {$ignore} INTO " . static::$table . " ($columns) VALUES ($values)";

            $instance->executeQuery($query);
        }

        return $instance->connection->insert_id;
    }

    public static function insertOrIgnore($data)
    {
        return self::insert($data, true);
    }

    public static function update($data)
    {
        $instance = self::getInstance();
        $setValues = [];

        foreach ($data as $column => $value) {
            $value = $instance->sanitizeValueAndQuote($value);
            $setValues[] = "{$column} = {$value}";
        }

        $setValues = implode(', ', $setValues);

        $conditions = $instance->conditions ? $instance->conditions . ' AND ' : '';
        $conditions .= "id = {$data['id']}";
        $query = "UPDATE " . static::$table . " SET {$setValues} WHERE {$conditions}";

        $result = $instance->executeQuery($query);

        return $result;
    }

    protected function sanitizeOperator($operator)
    {
        return $operator;
    }

    protected function sanitizeValue($value)
    {
        $escapedValue = $this->connection->real_escape_string($value);
        return $escapedValue;
    }

    protected function addCondition($condition)
    {
        if (empty($this->conditions)) {
            $this->conditions = " WHERE {$condition}";
        } else {
            $this->conditions .= " AND {$condition}";
        }

        return $this;
    }

    private function sanitizeValueAndQuote($value)
    {
        $escapedValue = $this->connection->real_escape_string($value);
        return "'{$escapedValue}'";
    }

    private static function getQuery()
    {
        $instance = self::getInstance();
        return "SELECT * FROM " . static::$table . " {$instance->conditions}";
    }

    private function executeQuery($query)
    {

        $result = $this->connection->query($query);

        if (!$result) {
            $this->dieWithError("Request execution error: " . $this->connection->error);
        }

        return $result;
    }

    private function dieWithError($errorMessage)
    {
        die($errorMessage);
    }

    private function normalizeData($data)
    {
        if (!is_array(reset($data))) {
            $data = [$data];
        }
        return $data;
    }
}
