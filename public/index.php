<?php

use Vendor\router\Router;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../web/routes.php';

$url = urldecode($_SERVER['REQUEST_URI']);
$publicPath = '/public';
$lastPublicPosition = strrpos($url, $publicPath);
if ($lastPublicPosition !== false) {
    $trimmedUrl = substr($url, $lastPublicPosition + strlen($publicPath));
    $trimmedUrl = trim($trimmedUrl, '/');
}

echo Router::dispatch($trimmedUrl, $_SERVER['REQUEST_METHOD']);

//$id = \App\Models\Currency::insert([
//    [
//        'code' => 'ghj',
//        'mid' => 0.100897,
//    ],
//    [
//        'code' => 'ghj',
//        'mid' => 0.100897,
//    ]
//]);
//$id = \App\Models\Currency::insert(
//    [
//        'code' => 'ghj',
//        'mid' => 444.100897,
//    ]
//);
//var_dump(\App\Models\Currency::update([
//        'id' => '1',
//        'code' => 'edf',
//        'mid' => 2.100897,
//    ]
//));
//var_dump((\App\Models\Currency::delete($id));
//var_dump(App\Models\Currency::get());

//var_dump(\App\Models\ConversionHistory::all());